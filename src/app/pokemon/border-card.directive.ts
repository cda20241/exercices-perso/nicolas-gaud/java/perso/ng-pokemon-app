import { Directive, ElementRef, HostListener, Renderer2, Input } from '@angular/core';

@Directive({
  selector: '[appBorderCard]'
})
export class BorderCardDirective {

  private initialColor: string = '#be7607';
  private defaultColor: string = '#185a0f';
  private defaultHeight: number = 200;

  constructor(private elt: ElementRef, private renderer: Renderer2) {
    this.setHeight(this.defaultHeight);
    this.setBorder(this.initialColor);
  }

  // see appBorderCard="red" in template
  @Input('appBorderCard') borderColor: string;

  @HostListener('mouseenter') onMouseEnter() {
    this.setBorder(this.borderColor || this.defaultColor);
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.setBorder(this.initialColor);
  }

  private setHeight(height: number) {
    this.elt.nativeElement.style.height = `${height}px`;
  }

  private setBorder(color: string) {
    this.elt.nativeElement.style.border = `4px solid ${color}`;
  }
}
